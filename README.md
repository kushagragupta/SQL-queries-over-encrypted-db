# A Secure Model to Execute Queries Over Encrypted Databases
Recently, cloud database services are being used by many users, mainly due to the enormous storage capacity and high computation speed. The users can get such a facility at a meager price by the cloud owners. However, privacy is still a concern for database owners since data is not under their control anymore. Encryption is the only way of satisfying user’s fears of data privacy, but executing SQL queries over encrypted information is challenging. Researchers have tried to address this privacy issue by encrypting the data using different encryption schemes, but the intruders can still attack the encrypted data.

# Modules -

1. Partitioning table(Original Table)
2. Partitioning tree
3. Encrypted table(AES)
4. Bits table(Encoding Algorithm)
5. Query Manager

*The project aims to design a secure model that allows end-users to run SQL queries over the encrypted database.<br/>
*Due to various drawbacks of earlier encryption mechanisms,  the model uses the AES encryption algorithm to encrypt the database table.<br/>
*The model's core components are the Query Manager, which will take the user's SELECT query, parse it to obtain the desired result and the Bits table will retrieve only part of the encrypted records that are candidates for the query.<br/>

# Reference 
S. Almakdi and B. Panda, "A Secure Model to Execute Queries Over Encrypted Databases in the Cloud," 2019 IEEE International Conference on Smart Cloud (SmartCloud), 2019, pp. 31-36, doi: 10.1109/SmartCloud.2019.00015.
